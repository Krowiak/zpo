import ast
import operator

class KolumnaNieIstniejeError(Exception):
    pass


class ZlyTypWartosciKolumnyError(Exception):
    pass


class OperacjaNieobslugiwanaError(Exception):
    pass


class Warunek(object):
    def __init__(self, operacja, lewa, prawa):
        self._lewa = lewa
        self._operacja = operacja
        self._prawa = prawa

    def sprawdz_dla(self, wiersz):
        lewa = self._lewa.sprawdz_dla(wiersz)
        prawa = self._prawa.sprawdz_dla(wiersz)
        return self._operacja(lewa, prawa)


class Stala(object):
    def __init__(self, wartosc):
        self._wartosc = wartosc

    def sprawdz_dla(self, wiersz):
        return self._wartosc


class Krotka(object):
    def __init__(self, kolumna):
        self._kolumna = kolumna

    def sprawdz_dla(self, wiersz):
        return wiersz[self._kolumna]


class OzdobnyWynik(object):
    _FORMAT_CALOSCI = "( {0})"
    _FORMAT_KOLUMNY = "{0}={1} "
    _FORMAT_STRINGA = "'{0}'"

    def __init__(self, wiersz, kolumny):
        self._definicje_kolumn = kolumny
        self.faktyczna_wartosc = wiersz

    def __str__(self):
        wynik = ""
        for nazwa, typ in self._definicje_kolumn.iteritems():
            wartosc = self.faktyczna_wartosc[nazwa]
            if typ is str:
                wartosc = self._FORMAT_STRINGA.format(wartosc)
            wynik += self._FORMAT_KOLUMNY.format(nazwa, wartosc)
        return self._FORMAT_CALOSCI.format(wynik)


class Relacja(object):
    def __init__(self, **kwargs):
        self._kolumny = kwargs
        self._wiersze = []

    def dopisz(self, **kwargs):
        nowy_wiersz = {}
        for klucz, wartosc in kwargs.iteritems():
            if klucz not in self._kolumny:
                raise KolumnaNieIstniejeError()
            if not self._kolumny[klucz] == type(wartosc):
                raise ZlyTypWartosciKolumnyError()
            nowy_wiersz[klucz] = wartosc
        self._wiersze.append(nowy_wiersz)

    def __iter__(self):
        i = 0
        while i < len(self._wiersze):
            yield OzdobnyWynik(self._wiersze[i], self._kolumny)
            i += 1

    def __call__(self, *args, **kwargs):
        zapytanie = args[0]
        skladowe = zapytanie.split("and")
        warunki = []

        for skladowa in skladowe:
            warunki.append(self._przetworz_skladowa(skladowa))

        do_sprawdzenia = self._wiersze
        for warunek in warunki:
            zgodne_wiersze = []
            for wiersz in do_sprawdzenia:
                if warunek.sprawdz_dla(wiersz):
                    zgodne_wiersze.append(wiersz)
            do_sprawdzenia = zgodne_wiersze

        wynik = Relacja(**self._kolumny)
        wynik._wiersze = do_sprawdzenia
        return wynik

    def _przetworz_skladowa(self, skladowa):
        skladowa = skladowa.strip()

        def przetworz(symbol, operacja):
            podskladowe = skladowa.split(symbol, 1)
            lewa = self._przetworz_argument(podskladowe[0])
            prawa = self._przetworz_argument(podskladowe[1])
            return Warunek(operacja, lewa, prawa)

        if "==" in skladowa:
            return przetworz("==", operator.eq)
        elif ">" in skladowa:
            return przetworz(">", operator.gt)
        elif "<" in skladowa:
            return przetworz("<", operator.lt)
        else:
            raise OperacjaNieobslugiwanaError()


    def _przetworz_argument(self, argument):
        argument = argument.strip()
        if argument in self._kolumny:
            return Krotka(argument)
        else:
            return Stala(ast.literal_eval(argument))

if __name__ == "__main__":
    osoby = Relacja(nazwisko=str, wiek=int)
    osoby.dopisz(nazwisko='Nowak', wiek=19)
    osoby.dopisz(nazwisko='Nowak', wiek=20)
    osoby.dopisz(nazwisko='Kowalski', wiek=20)
    print("///// Wszyscy:")
    for k in osoby:
        print k
    odp1 = osoby("wiek == 20")
    print("///// Dwudziestolecie:")
    for k in odp1:
        print k
    odp2 = osoby("nazwisko == 'Nowak' and wiek < 20")
    print("///// Nowak Junior:")
    for k in odp2:
        print k
