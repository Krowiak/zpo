class Kolejka(object):
    def __init__(self):
        self._wartosci = []

    def __lshift__(self, wartosc):
        if len(self._wartosci) == 0:
            self._wartosci.append(wartosc)
        else:
            wstawiono = False
            for i in range(0, len(self._wartosci)):
                if self._wartosci[i] < wartosc:
                    self._wartosci.insert(i, wartosc)
                    wstawiono = True
                    break
            if not wstawiono:
                self._wartosci.append(wartosc)
        return self

    def __len__(self):
        return len(self._wartosci)

    def getmax(self):
        return self._wartosci.pop(0)

if __name__ == "__main__":
    k = Kolejka()
    k << 2 << 5 << 1
    print k.getmax()
    k << 3
    print k.getmax(), k.getmax(), k.getmax()
    print len(k)
