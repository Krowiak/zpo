class Permutacja(object):
    def __init__(self, *ciag):
        self.ciag = ciag

    def naCykle(self):
        cykle = []
        niezbadane = list(self.ciag)
        while len(niezbadane) > 0:
            akt_cykl = []
            akt_element = niezbadane[-1]
            while True:
                akt_cykl.append(akt_element)
                if self.ciag[akt_element - 1] == akt_cykl[0]:
                    break
                else:
                    akt_element = self.ciag[akt_element - 1]
            for i in akt_cykl:
                niezbadane.remove(i)
            cykle.append(tuple(akt_cykl))
        return cykle

    def __call__(self, *args, **kwargs):
        liczba = args[0]
        return self.ciag[liczba - 1]

if __name__ == "__main__":
    p = Permutacja(1,4,5,2,3)
    print(p.naCykle())
    print(p(3))
