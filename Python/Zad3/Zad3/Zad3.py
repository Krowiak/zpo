import math

class Zespolona(complex):
    def __lt__(self, ob):
        self_kat = math.atan2(self.imag, self.real)
        ob_kat = math.atan2(ob.imag, ob.real)
        return self_kat < ob_kat

print(Zespolona(2+1j) < Zespolona(1+2j))