class Drzewo(object):
    
    def __init__(self):
        self.__synowie = []
        
    def dodajSyna(self, syn):
        self.__synowie.append(syn)
        
    def drukuj(self, poziom=0):
        print (poziom*" ", "o")
        for syn in self.__synowie:
            syn.drukuj(poziom+1)


def wyswietlSlowoDycka(slowo):
    stos = []
    dlugosc_slowa = len(slowo)
    for i in range(0, dlugosc_slowa):
        if slowo[i] == 'x':
            d = Drzewo()

            while i < dlugosc_slowa - 1 and slowo[i+1] == "y":
                d.dodajSyna(stos.pop())
                i += 1

            stos.append(d)

    stos[0].drukuj()

if __name__ == "__main__":
    wyswietlSlowoDycka("xxyxxxxyyxyyy")
