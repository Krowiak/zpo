# -*- coding: utf-8 -*-

import itertools as it
import ast
import operator as op


class Rownanie(object):
    """
    Inspiracja: https://stackoverflow.com/questions/2371436/evaluating-a-mathematical-expression-in-a-string/9558001#9558001
    """
    _OPERATORY = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                  ast.USub: op.neg}

    def __init__(self, lewa, prawa):
        self.lewa = lewa
        self.prawa = prawa

    def _oblicz_wyrazenie(self, expr):
        return self._oblicz(ast.parse(expr, mode='eval').body)

    def _oblicz(self, node):
        if isinstance(node, ast.Num):  # Po prostu liczba, np. 3
            return node.n
        elif isinstance(node, ast.BinOp):  # lewa (op) prawa, np. 3*2
            return self._OPERATORY[type(node.op)](self._oblicz(node.left), self._oblicz(node.right))
        elif isinstance(node, ast.UnaryOp):  # (op) wartosc, np. -1
            return self._OPERATORY[type(node.op)](self._oblicz(node.operand))
        else:
            raise TypeError(node)

    def sprawdz_dla(self, wartosci_zmiennych):
        s_lewa = self.lewa.zloz_rownanie(wartosci_zmiennych)
        # print(s_lewa + " ??? " + str(self.prawa))
        # print(self._oblicz_wyrazenie(s_lewa) == self.prawa)
        # print("///")
        return self._oblicz_wyrazenie(s_lewa) == self.prawa


class Wezel(object):
    def __init__(self, lewa, prawa, operacja):
        self.lewa = lewa
        self.prawa = prawa
        self.operacja = operacja

    def zloz_rownanie(self, wartosci):
        if type(self.lewa) == Zmienna:
            s_lewa = str(self._wart_zmiennej(wartosci, self.lewa))
        elif type(self.lewa) == Wezel:
            s_lewa = self.lewa.zloz_rownanie(wartosci)
        else:
            s_lewa = str(self.lewa)

        if type(self.prawa) == Zmienna:
            s_prawa = str(self._wart_zmiennej(wartosci, self.prawa))
        elif type(self.prawa) == Wezel:
            s_prawa = self.prawa.zloz_rownanie(wartosci)
        else:
            s_prawa = str(self.prawa)

        return s_lewa + self.operacja + s_prawa

    def _wart_zmiennej(self, wartosci, zmienna):
        for para in wartosci:
            wartosc, symbol = para
            if symbol == zmienna.symbol:
                return wartosc

    def __eq__(self, other):
        return Rownanie(self, other)

    def __add__(self, other):
        return Wezel(self, other, "+")

    def __radd__(self, other):
        return Wezel(other, self, "+")

    def __mul__(self, other):
        return Wezel(self, other, "*")

    def __rmul__(self, other):
        return Wezel(other, self, "*")

    def __sub__(self, other):
        return Wezel(self, other, "-")

    def __rsub__(self, other):
        return Wezel(other, self, "-")

    def __neg__(self):
        return Wezel("", self, "-")


class Zmienna(object):
    def __init__(self, symbol, wartosci):
        self.symbol = symbol
        self.wartosci = wartosci

    def __eq__(self, other):
        return Rownanie(Wezel("", self, ""), other)

    def __add__(self, other):
        return Wezel(self, other, "+")

    def __radd__(self, other):
        return Wezel(other, self, "+")

    def __mul__(self, other):
        return Wezel(self, other, "*")

    def __rmul__(self, other):
        return Wezel(other, self, "*")

    def __sub__(self, other):
        return Wezel(self, other, "-")

    def __rsub__(self, other):
        return Wezel(other, self, "-")

    def __neg__(self):
        return Wezel("", self, "-")


class UkladRownan(object):
    def __init__(self, *args):
        self._zmienne = args
        self._rownania = []
        self._niespelnialne = False

    def __iadd__(self, other):
        if type(other) == bool:
            if not other:
                self._niespelnialne = True
        else:
            self._rownania.append(other)
        return self

    def rozwiaz(self):
        if self._niespelnialne:
            return []

        rozwiazania = []
        dozwolone_wartosci = []

        for zmienna in self._zmienne:
            dozwolone_wartosci.append(list(zmienna.wartosci))

        for zestaw_wartosci in it.product(*dozwolone_wartosci):
            mozliwosc = zip(zestaw_wartosci, map(lambda zmienna: zmienna.symbol, self._zmienne))
            if all(map(lambda rownanie: rownanie.sprawdz_dla(mozliwosc), self._rownania)):
                rozwiazania.append(mozliwosc)

        return rozwiazania

if __name__ == "__main__":
    # x = Zmienna('x', frozenset(range(1,10)))
    # y = Zmienna('y', frozenset([2,3,7]))
    # ur = UkladRownan(x, y)
    # ur += 2*x - 7*y == 3
    # ur += -x + 2*y == -9
    # x = Zmienna('x', range(1, 10))
    # y = Zmienna('y', range(1, 10))
    # z = Zmienna('z', [5, 6])
    # ur = UkladRownan(x, y, z)
    # #ur += 1 == 2
    # ur += x - y == 1
    # ur += 2*y == 2
    # ur += x - x == 0
    # ur += y - x == -1
    # ur += 2 == 2
    # ur += z == 5
    x = Zmienna('x', range(1, 10))
    y = Zmienna('y', range(1, 10))
    z = Zmienna('z', [2,4,8,10])
    ur = UkladRownan(x, y, z)
    ur += x - y == 1
    ur += y - z == 1

    for odp in ur.rozwiaz():
        print odp
