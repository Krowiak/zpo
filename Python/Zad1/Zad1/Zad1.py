class Tablice(object):
    def __init__(self, pocz):
        self.pocz = pocz

    def __iter__(self):
        tmp = self.pocz - 1
        lista = []
        while True:
            tmp += 1
            lista.append(tmp)
            yield lista

t = Tablice(3)
for i in t:
    print(i)
    if len(i) >= 4:
        break

t.pocz = 9
for i in t:
    print(i)
    if len(i) >= 2:
        break