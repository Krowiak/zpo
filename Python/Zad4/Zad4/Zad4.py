class ZbiorPowt(object):
    def __init__(self):
        self._elems = dict()

    def __str__(self):
        return str(self._elems)

    def dodaj_elem(self, elem, razy=1):
        if razy < 1: return
        if self._elems.get(elem) is None:
            self._elems[elem] = razy
        else:
            self._elems[elem] += razy

    def usun_elem(self, elem, razy=1):
        if self._elems.get(elem) is None:
            pass
        else:
            self._elems[elem] -= razy
            if self._elems[elem] < 1:
                del self._elems[elem]

    def _tworz(self, zbior, fun_licz):
        wynik = ZbiorPowt()
        elementy = set(self._elems.keys()).union(zbior._elems.keys())
        for x in elementy:
            self_licz = self._elems.get(x, 0)
            drugi_licz = zbior._elems.get(x, 0)
            licz = fun_licz(self_licz, drugi_licz)
            wynik.dodaj_elem(x, licz)
        return wynik

    def suma(self, zbior):
        return self._tworz(zbior, max)

    def iloczyn(self, zbior):
        return self._tworz(zbior, min)

    def roznica(self, zbior):
        return self._tworz(zbior, lambda x, y: max(x - y, 0))

a = ZbiorPowt()
a.dodaj_elem("a", 5)
a.usun_elem("a", 2)
a.dodaj_elem("b")
a.dodaj_elem("b")
a.dodaj_elem("b")
a.usun_elem("b")
a.usun_elem("c", 13)

b = ZbiorPowt()
b.dodaj_elem("c", 2)
b.dodaj_elem("a")
b.dodaj_elem("b", 11)

print(str.format("a: {0}", a))
print(str.format("b: {0}", b))
print(str.format("a + b: {0}", a.suma(b)))
print(str.format("a * b: {0}", a.iloczyn(b)))
print(str.format("a - b: {0}", a.roznica(b)))
print(str.format("b + a: {0}", b.suma(a)))
print(str.format("b * a: {0}", b.iloczyn(a)))
print(str.format("b - a: {0}", b.roznica(a)))
