import itertools as it

class Graf(object):
    _STR_FORMAT = "({{{0}}}, {{{1}}})"

    def __init__(self, wierzcholki, krawedzie):
        self._wierzcholki = wierzcholki
        self._krawedzie = krawedzie

# 1 2 3, 1-2, 2-3
    def __str__(self):
        return self._STR_FORMAT.format(','.join(map(str, self._wierzcholki)), ', '.join(map(str, self._krawedzie)))

    def __iter__(self):
        for licz_wierz in range(1, len(self._wierzcholki) + 1):
            wierzcholki = it.combinations(self._wierzcholki, licz_wierz)
            for akt_wierzcholki in wierzcholki:
                zest_krawedzi = self._zestawy_krawedzi_dla(akt_wierzcholki)
                for zestaw in zest_krawedzi:
                    yield Graf(akt_wierzcholki, zestaw)


    def _zestawy_krawedzi_dla(self, wierzcholki):
        mozliwe_krawedzie = self._krawedzie_dla(wierzcholki)
        zestawy_krawedzi = []
        for licz_krawedzi in range(len(mozliwe_krawedzie) + 1):
            for zestaw in it.combinations(mozliwe_krawedzie, licz_krawedzi):
                zestawy_krawedzi.append(zestaw)
        return zestawy_krawedzi


    def _krawedzie_dla(self, wierzcholki):
        krawedzie = []
        kombinacje = it.combinations(wierzcholki, 2)
        for kombinacja in kombinacje:
            krawedz = self._krawedz(kombinacja)
            if krawedz is not None:
                krawedzie.append(krawedz)
        return krawedzie


    def _krawedz(self, wierzcholki):
        if (wierzcholki[0], wierzcholki[1]) in self._krawedzie:
            return (wierzcholki[0], wierzcholki[1])
        elif (wierzcholki[1], wierzcholki[0]) in self._krawedzie:
            return (wierzcholki[1], wierzcholki[0])
        else:
            return None

g = Graf(set([1,2,3]), set([(1,2), (2,3), (3,1)]))
for podgraf in g:
    print podgraf