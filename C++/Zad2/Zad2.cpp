#include "Czas.h"
#include <iostream>

using namespace std;

int main()
{
	Czas cz(11, 42, 5);
	cout << "Godzina: " << cz.podajGodzine() << ", minuta: " << cz.podajMinute() << ", sekunda: " << cz.podajSekunde();
	cin.get();
	return 0;
}