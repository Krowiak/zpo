#pragma once
class Czas
{
private:
	int godzina;
	int minuta;
	int sekunda;

public:
	Czas(int godzina, int minuta, int sekunda)
	{
		ustawGodzine(godzina);
		ustawMinute(minuta);
		ustawSekunde(sekunda);
	}

	Czas() : Czas(0, 0, 0)
	{
	}

	int podajGodzine()
	{
		return godzina;
	}

	int podajMinute()
	{
		return minuta;
	}

	int podajSekunde()
	{
		return sekunda;
	}

	void ustawGodzine(int godzina)
	{
		this->godzina = godzina;
	}

	void ustawMinute(int minuta)
	{
		this->minuta = minuta;
	}

	void ustawSekunde(int sekunda)
	{
		this->sekunda = sekunda;
	}

};