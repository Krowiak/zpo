#pragma once
class Czas
{
private:
	int godzina;
	int minuta;
	int sekunda;

public:
	Czas(int godzina, int minuta, int sekunda)
	{
		ustawGodzine(godzina);
		ustawMinute(minuta);
		ustawSekunde(sekunda);
	}

	Czas() : Czas(0, 0, 0)
	{
	}

	int podajGodzine()
	{
		return godzina;
	}

	int podajMinute()
	{
		return minuta;
	}

	int podajSekunde()
	{
		return sekunda;
	}

	virtual void ustawGodzine(int godzina)
	{
		this->godzina = godzina;
	}

	virtual void ustawMinute(int minuta)
	{
		this->minuta = minuta;
	}

	virtual void ustawSekunde(int sekunda)
	{
		this->sekunda = sekunda;
	}

};

class CzasZKontrola : public Czas
{
public:
	CzasZKontrola() : CzasZKontrola(0, 0, 0)
	{ }

	CzasZKontrola(int godzina, int minuta, int sekunda)
	{ 
		ustawGodzine(godzina);
		ustawMinute(minuta);
		ustawSekunde(sekunda);
	}

	void ustawGodzine(int godzina)
	{
		godzina = godzina >= 0 ? godzina : 0;
		godzina = godzina <= 23 ? godzina : 23;
		Czas::ustawGodzine(godzina);
	}

	void ustawMinute(int minuta)
	{
		minuta = minuta >= 0 ? minuta : 0;
		minuta = minuta <= 59 ? minuta : 59;
		Czas::ustawMinute(minuta);
	}

	void ustawSekunde(int sekunda)
	{
		sekunda = sekunda >= 0 ? sekunda : 0;
		sekunda = sekunda <= 59 ? sekunda : 59;
		Czas::ustawSekunde(sekunda);
	}
};