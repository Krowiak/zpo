#include "CzasZKontrola.h"
#include <iostream>

using namespace std;

int main()
{
	CzasZKontrola cz(11, 42, 99);
	cz.ustawGodzine(88);
	cout << "Godzina: " << cz.podajGodzine() << ", minuta: " << cz.podajMinute() << ", sekunda: " << cz.podajSekunde();
	cin.get();
	return 0;
}