#include "Kolo.h"
#include "Kwadrat.h"
#include "Prostokat.h"
#include "Trapez.h"
#include <iostream>

using namespace std;

int main()
{
	Kolo kolo(3);
	Kwadrat kwadrat;
	Prostokat prostokat(2, 13);
	Trapez trapez(0, 2, 3);

	cout << "Pole kola: " << kolo.obliczPole() << endl
		<< "Pole kwadratu: " << kwadrat.obliczPole() << endl
		<< "Pole prostokata: " << prostokat.obliczPole() << endl
		<< "Pole trapezu: " << trapez.obliczPole() << endl;

	system("PAUSE");
	return 0;
}