#pragma once

class Prostokat
{
public:
	Prostokat() : Prostokat(1, 1)
	{ }

	Prostokat(double bokA, double bokB)
	{
		ustawBokA(bokA);
		ustawBokB(bokB);
	}

	double obliczPole()
	{
		return bokA * bokB;
	}

	double pobierzBokA()
	{
		return bokA;
	}
	double pobierzBokB()
	{
		return bokB;
	}

	void ustawBokA(double bokA)
	{
		this->bokA = bokA > 0 ? bokA : 1;
	}
	void ustawBokB(double bokB)
	{
		this->bokB = bokB > 0 ? bokB : 1;
	}

private:
	double bokA;
	double bokB;
};