#pragma once

class Trapez
{
public:
	Trapez() : Trapez(1, 1, 1)
	{ }

	Trapez(double podstA, double podstB, double wysokosc)
	{ 
		ustawPodstaweA(podstA);
		ustawPodstaweB(podstB);
		ustawWysokosc(wysokosc);
	}

	double obliczPole()
	{
		return (podstawaA + podstawaB) * wysokosc / 2;
	}

	double pobierzPodstaweA()
	{
		return podstawaA;
	}
	double pobierzPodstaweB()
	{
		return podstawaA;
	}
	double pobierzWysokosc()
	{
		return wysokosc;
	}

	void ustawPodstaweA(double podstawaA)
	{
		this->podstawaA = podstawaA > 0 ? podstawaA : 1;
	}
	void ustawPodstaweB(double podstawaB)
	{
		this->podstawaB = podstawaB > 0 ? podstawaB : 1;
	}
	void ustawWysokosc(double wysokosc)
	{
		this->wysokosc = wysokosc > 0 ? wysokosc : 1;
	}

private:
	double podstawaA;
	double podstawaB;
	double wysokosc;
};