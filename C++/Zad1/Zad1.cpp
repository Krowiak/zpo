#include "Data.h"
#include <iostream>

using namespace std;

int main()
{
	Data d(1, 12, 2000);
	cout << "Dzien: " << d.podajDzien() << ", miesiac: " << d.podajMiesiac() << ", rok: " << d.podajRok();
	cin.get();
	return 0;
}