#pragma once
class Data
{
private:
	int dzien;
	int miesiac;
	int rok;

public:
	Data(int dzien, int miesiac, int rok)
	{
		ustawDzien(dzien);
		ustawMiesiac(miesiac);
		ustawRok(rok);
	}

	Data() : Data(1, 1, 1)
	{
	}

	int podajDzien()
	{
		return dzien;
	}

	int podajMiesiac()
	{
		return miesiac;
	}

	int podajRok()
	{
		return rok;
	}

	void ustawDzien(int dzien)
	{
		this->dzien = dzien;
	}

	void ustawMiesiac(int miesiac)
	{
		this->miesiac = miesiac;
	}

	void ustawRok(int rok)
	{
		this->rok = rok;
	}

};

