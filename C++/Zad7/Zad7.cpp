#include "Punkt3D.h"
#include <iostream>

using namespace std;

int main()
{
	Punkt3D p(15, 20, 3);
	cout << "x: " << p.pobierzX() << ", y: " << p.pobierzY() << ", z: " << p.pobierzZ() << endl;
	p.ustawX(16);
	p.ustawY(81);
	p.ustawZ(99);
	cout << "Nowy x: " << p.pobierzX() << ", nowy y: " << p.pobierzY() << ", nowy z: " << p.pobierzZ();
	cin.get();
	return 0;
}