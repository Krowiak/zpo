#pragma once
class Punkt
{
private:
	int x;
	int y;

public:
	Punkt(int x, int y)
	{
		ustawX(x);
		ustawY(y);
	}

	Punkt() : Punkt(0, 0)
	{
	}

	int pobierzX()
	{
		return x;
	}

	int pobierzY()
	{
		return y;
	}

	void ustawX(int x)
	{
		this->x = x;
	}

	void ustawY(int y)
	{
		this->y = y;
	}
};

class Punkt3D : public Punkt
{
private:
	int z;

public:
	Punkt3D(int x, int y, int z) : Punkt(x, y)
	{
		ustawZ(z);
	}

	Punkt3D() : Punkt()
	{ }

	int pobierzZ()
	{
		return z;
	}

	void ustawZ(int z)
	{
		this->z = z;
	}
};