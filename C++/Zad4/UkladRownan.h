#pragma once
#include "Rownanie.h"
#include <iostream>

using namespace std;

class UkladRownan
{
private:
	Rownanie r1;
	Rownanie r2;

public:
	UkladRownan(Rownanie rownanie1, Rownanie rownanie2) : r1(rownanie1), r2(rownanie2)
	{	}

	void rozwiaz()
	{
		double w = r1.podajA() * r2.podajB() - r1.podajB() * r2.podajA();
		double wx = r1.podajC() * r2.podajB() - r1.podajB() * r2.podajC();
		double wy = r1.podajA() * r2.podajC() - r1.podajC() * r2.podajA();

		if (w != 0)  // uk�ad oznaczony
		{
			cout << "x = " << wx / w << ", y = "  << wy / w << ". " << endl;
		}
		else if (wx == 0 && wy == 0)  // uk�ad nieoznaczony
		{
			cout << "Nieskonczenie wiele rozwiazan." <<endl;
		}
		else  // uk�ad sprzeczny
		{
			cout << "Brak rozwiazan." << endl;
		}
	}

	Rownanie pobierzRownanie1()
	{
		return r1;
	}
	Rownanie pobierzRownanie2()
	{
		return r2;
	}

	void ustawRownanie1(Rownanie r)
	{
		r1 = r;
	}
	void ustawRownanie2(Rownanie r)
	{
		r2 = r;
	}
};