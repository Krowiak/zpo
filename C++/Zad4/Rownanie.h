#pragma once

class Rownanie
{
private:
	double a;
	double b;
	double c;

public:
	Rownanie(double a, double b, double c)
	{
		ustawA(a);
		ustawB(b);
		ustawC(c);
	}

	double podajA()
	{
		return a;
	}
	double podajB()
	{
		return b;
	}
	double podajC()
	{
		return c;
	}

	void ustawA(double a)
	{
		this->a = a;
	}
	void ustawB(double b)
	{
		this->b = b;
	}
	void ustawC(double c)
	{
		this->c = c;
	}
};
