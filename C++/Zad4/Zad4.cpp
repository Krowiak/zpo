#include "UkladRownan.h"
#include <iostream>

using namespace std;

int main()
{
	int a, b, c;
	cout << "Rownania w formacie ax + by = c." << endl << "Pierwsze rownanie: " << endl << "Podaj a: ";
	cin >> a;
	cout << endl << "Podaj b: ";
	cin >> b;
	cout << endl << "Podaj c: ";
	cin >> c;
	Rownanie r1(a, b, c);

	cout << endl << "Drugie rownanie: " << endl << "Podaj a: ";
	cin >> a;
	cout << endl << "Podaj b: ";
	cin >> b;
	cout << endl << "Podaj c: ";
	cin >> c;
	Rownanie r2(a, b, c);

	UkladRownan uk(r1, r2);
	cout << endl << endl << "Wynik: ";
	uk.rozwiaz();
	system("PAUSE");
	return 0;
}