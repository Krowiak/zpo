#pragma once
class Punkt
{
private:
	int x;
	int y;

public:
	Punkt(int x, int y)
	{
		ustawX(x);
		ustawY(y);
	}

	Punkt() : Punkt(0, 0)
	{
	}

	int pobierzX()
	{
		return x;
	}

	int pobierzY()
	{
		return y;
	}

	void ustawX(int x)
	{
		this->x = x;
	}

	void ustawY(int y)
	{
		this->y = y;
	}
};