#include "Punkt.h"
#include <iostream>

using namespace std;

int main()
{
	Punkt p(15, 20);
	cout << "x: " << p.pobierzX() << ", y: " << p.pobierzY() << endl;
	p.ustawX(16);
	p.ustawY(81);
	cout << "Nowy x: " << p.pobierzX() << ", nowy y: " << p.pobierzY();
	cin.get();
	return 0;
}