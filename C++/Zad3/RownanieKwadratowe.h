#pragma once
#include <math.h>

class RownanieKwadratowe
{
private:
	int A;
	int B;
	int C;

public:
	RownanieKwadratowe(int a, int b, int c)
	{
		ustawA(a);
		ustawB(b);
		ustawC(c);
	}

	RownanieKwadratowe() : RownanieKwadratowe(0, 0, 0)
	{
	}

	int podajA()
	{
		return A;
	}

	int podajB()
	{
		return B;
	}

	int podajC()
	{
		return C;
	}

	void ustawA(int a)
	{
		A = a;
	}

	void ustawB(int b)
	{
		B = b;
	}

	void ustawC(int c)
	{
		C = c;
	}

	double delta()
	{
		return (double)(B*B - 4 * A*C);
	}

	double obliczX1()
	{
		return -(B - sqrt(delta())) / 2 * A;
	}

	double obliczX2()
	{
		return -(B + sqrt(delta())) / 2 * A;
	}

	double obliczX12()
	{
		return -B / 2 * (double)A;
	}

	double obliczXFunkcjiLiniowej()
	{
		return -B / (double)C;
	}
};
