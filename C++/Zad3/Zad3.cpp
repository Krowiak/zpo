#include "RownanieKwadratowe.h"
#include <iostream>

using namespace std;

int main()
{
	RownanieKwadratowe r;
	double num;
	cout << "Podaj A : "; cin >> num; r.ustawA(num);
	cout << "Podaj B : "; cin >> num; r.ustawB(num);
	cout << "Podaj C : "; cin >> num; r.ustawC(num);
	if (r.podajA() == 0)
	{
		if (r.podajC() == 0)
		{
			if (r.podajB() == 0)
			{
				cout << "R�wnanie liniowe o niesko�czonej liczbie miejc zerowych.";
			}
			else
			{
				cout << "R�wnanie liniowe bez miejsc zerowych.";
			}
		}
		else
		{
			cout << "R�wnanie liniowe. Miejsce zerowe x=" << r.obliczXFunkcjiLiniowej();
		}
	}
	else if (r.delta() > 0)
		cout << "Pierwiastki rownania x1=" << r.obliczX1() << " x2="
		<< r.obliczX2() << endl;
	else
		if (r.delta() == 0)
			cout << "Pierwiastek podw�jny x12=" << r.obliczX12() << endl;
		else
			cout << "Brak pierwiastkow rzeczywistych" << endl;
	system("PAUSE");
	return 0;
}