#pragma once
class Data
{
private:
	int dzien;
	int miesiac;
	int rok;

public:
	Data(int dzien, int miesiac, int rok)
	{
		ustawDzien(dzien);
		ustawMiesiac(miesiac);
		ustawRok(rok);
	}

	Data() : Data(1, 1, 1)
	{
	}

	int podajDzien()
	{
		return dzien;
	}

	int podajMiesiac()
	{
		return miesiac;
	}

	int podajRok()
	{
		return rok;
	}

	virtual void ustawDzien(int dzien)
	{
		this->dzien = dzien;
	}

	virtual void ustawMiesiac(int miesiac)
	{
		this->miesiac = miesiac;
	}

	virtual void ustawRok(int rok)
	{
		this->rok = rok;
	}

};

class DataZKontrola : public Data
{
private:
	bool rokJestPrzestepny()
	{
		int rok = podajRok();
		return rok % 400 == 0 || (rok % 4 == 0 && rok % 100 != 0);
	}

	int ostatniDzienMiesiaca()
	{
		switch (podajMiesiac())
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 2:
			return rokJestPrzestepny() ? 29 : 28;
		default:
			return 30;
		}
	}

public:
	DataZKontrola(int dzien, int miesiac, int rok)
	{
		ustawRok(rok);
		ustawMiesiac(miesiac);
		ustawDzien(dzien);
	}

	DataZKontrola() : DataZKontrola(1, 1, 1)
	{
	}

	void ustawDzien(int dzien)
	{
		dzien = dzien > 0 ? dzien : 1;
		int maksDzien = ostatniDzienMiesiaca();
		dzien = dzien <= maksDzien ? dzien : maksDzien;
		Data::ustawDzien(dzien);
	}

	void ustawMiesiac(int miesiac)
	{
		miesiac = miesiac > 0 ? miesiac : 1;
		miesiac = miesiac <= 12 ? miesiac : 12;
		Data::ustawMiesiac(miesiac);
	}

	void ustawRok(int rok)
	{
		Data::ustawRok(rok);
	}

};

