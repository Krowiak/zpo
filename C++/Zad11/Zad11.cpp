#include <iostream>
#include "Kolo.h"
#include "Kwadrat.h"
#include "Prostokat.h"
#include "Trapez.h"
#include "Figura.h"

using namespace std;

int main()
{
	Kwadrat kw(2);
	Kolo ko(1);
	Prostokat pr(3, 2);
	Figura * f;
	f = &kw;
	f->wypiszNazwe();
	cout << ' ' << f->obliczPole() << endl;
	f = &ko;
	f->wypiszNazwe();
	cout << ' ' << f->obliczPole() << endl;
	f = &pr;
	f->wypiszNazwe();
	cout << ' ' << f->obliczPole() << endl;

	system("PAUSE");
	return 0;
}