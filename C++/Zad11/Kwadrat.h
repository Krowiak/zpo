#pragma once
#include <iostream>
#include "Figura.h"

class Kwadrat : public Figura
{
public:
	Kwadrat() : Kwadrat(1)
	{ }

	Kwadrat(double bok)
	{
		ustawBok(bok);
	}

	double obliczPole()
	{
		return bok * bok;
	}

	void wypiszNazwe()
	{
		std::cout << "Kwadrat";
	}

	double pobierzBok()
	{
		return bok;
	}

	void ustawBok(double bok)
	{
		this->bok = bok > 0 ? bok : 1;
	}

private:
	double bok;
};