#pragma once
#include <iostream>

class Figura
{
public:
	Figura()
	{ }

	virtual double obliczPole()
	{
		return 0;
	}

	virtual void wypiszNazwe()
	{
		std::cout << "Figura";
	}
};