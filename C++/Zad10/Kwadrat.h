#pragma once

class Kwadrat
{
public:
	Kwadrat() : Kwadrat(1)
	{ }

	Kwadrat(double bok)
	{
		ustawBok(bok);
	}

	virtual double obliczPole()
	{
		return bok * bok;
	}

	double pobierzBok()
	{
		return bok;
	}

	void ustawBok(double bok)
	{
		this->bok = bok > 0 ? bok : 1;
	}

private:
	double bok;
};