#pragma once
#include"Prostokat.h"

class Prostopadloscian : public Prostokat
{
public:
	Prostopadloscian() : Prostopadloscian(1, 1, 1)
	{ }

	Prostopadloscian(double podstawaA, double podstawaB, double wysokosc) : Prostokat(podstawaA, podstawaB)
	{
		ustawWysokosc(wysokosc);
	}

	double obliczPole()
	{
		return 2 * Prostokat::obliczPole() + 
			2 * pobierzBokA() * wysokosc +
			2 * pobierzBokB() * wysokosc;
	}

	double pobierzWysokosc()
	{
		return wysokosc;
	}

	void ustawWysokosc(double wysokosc)
	{
		this->wysokosc = wysokosc > 0 ? wysokosc : 1;
	}

private:
	double wysokosc;
};