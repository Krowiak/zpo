#pragma once
#include "Kwadrat.h"

class Szescian : public Kwadrat
{
public:
	Szescian() : Szescian(1)
	{ }

	Szescian(double bok) : Kwadrat(bok)
	{ }

	double obliczPole()
	{
		return 6 * Kwadrat::obliczPole();
	}
};