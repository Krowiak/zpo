#include <iostream>
#include "GraniastoslupOPodstawieTrapezu.h"
#include "Kula.h"
#include "Prostopadloscian.h"
#include "Szescian.h"

using namespace std;

int main()
{
	Kula kula(5);
	Szescian szescian(2);
	Prostopadloscian prostopadloscian(4, 8, 7);
	GraniastoslupOPodstawieTrapezu graniastoslupOPodstawieTrapezu;

	cout << "Pole kuli: " << kula.obliczPole() << endl
		<< "Pole szescianu: " << szescian.obliczPole() << endl
		<< "Pole prostopadloscianu: " << prostopadloscian.obliczPole() << endl
		<< "Pole graniastoslupa o podstawie trapezu: " << graniastoslupOPodstawieTrapezu.obliczPole() << endl;

	system("PAUSE");
	return 0;
}