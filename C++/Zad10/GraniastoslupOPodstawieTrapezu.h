#pragma once
#include "Trapez.h"

class GraniastoslupOPodstawieTrapezu : public Trapez
{
public:
	GraniastoslupOPodstawieTrapezu() : GraniastoslupOPodstawieTrapezu(1, 1, 1, 1)
	{ }

	GraniastoslupOPodstawieTrapezu(double podstA, double podstB, double wysPodstawy, double wysBryly) : Trapez(podstA, podstB, wysPodstawy)
	{
		ustawWysokoscBryly(wysokoscBryly);
	}

	double obliczPole()
	{
		return 2 * Trapez::obliczPole() +
			2 * pobierzPodstaweA() * wysokoscBryly +
			2 * pobierzPodstaweB() * wysokoscBryly;
	}

	double pobierzWysokoscBryly()
	{
		return wysokoscBryly;
	}

	void ustawWysokoscBryly(double wysokoscBryly)
	{
		this->wysokoscBryly = wysokoscBryly > 0 ? wysokoscBryly : 1;
	}

private:
	double wysokoscBryly;
};