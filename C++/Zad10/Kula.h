#pragma once
#include "Kolo.h"

class Kula : public Kolo
{
public:
	Kula() : Kula(1)
	{ }

	Kula(double przekatna) : Kolo(przekatna)
	{ }

	double obliczPole()
	{
		return 4 * Kolo::obliczPole();
	}
};