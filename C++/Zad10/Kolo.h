#pragma once
#define _USE_MATH_DEFINES
#include <math.h>

class Kolo
{
public:
	Kolo() : Kolo(1)
	{ }

	Kolo(double przekatna)
	{
		ustawPrzekatna(przekatna);
	}

	virtual double obliczPole()
	{
		return M_PI * przekatna * przekatna;
	}

	double pobierzPrzekatna()
	{
		return przekatna;
	}

	void ustawPrzekatna(double przekatna)
	{
		this->przekatna = przekatna > 0 ? przekatna : 1;
	}

private:
	double przekatna;
};