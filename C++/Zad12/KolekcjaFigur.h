#pragma once
#include "Figura.h"

class KolekcjaFigur
{
public:
	KolekcjaFigur()
	{ 
		nastIndeks = 0;
		wyczysc();
	}

	void dodajFigure(Figura* doDodania)
	{
		tablicaFigur[nastIndeks] = doDodania;
		nastIndeks++;
	}

	void wyczysc()
	{
		for (int i = 0; i < ROZMIAR_KOLEKCJI; i++)
		{
			tablicaFigur[i] = NULL;
		}
		nastIndeks = 0;
	}

	void kasuj()
	{
		for (int i = 0; i < nastIndeks; i++)
		{
			delete tablicaFigur[i];
		}
		wyczysc();
	}

	double obliczSumarycznePole()
	{
		double sumarycznePole = 0;
		for (int i = 0; i < nastIndeks; i++)
		{
			sumarycznePole += tablicaFigur[i]->obliczPole();
		}
		return sumarycznePole;
	}

private:
	static const int ROZMIAR_KOLEKCJI = 314;
	Figura * tablicaFigur[ROZMIAR_KOLEKCJI];
	int nastIndeks;
};