#include <iostream>
#include "Kolo.h"
#include "Kwadrat.h"
#include "Prostokat.h"
#include "Trapez.h"
#include "Figura.h"
#include "KolekcjaFigur.h"

using namespace std;

int main()
{
	KolekcjaFigur kf;
	kf.dodajFigure(new Kwadrat);
	kf.dodajFigure(new Kolo);
	kf.dodajFigure(new Prostokat);
	cout << kf.obliczSumarycznePole() << endl;
	kf.kasuj();
	kf.kasuj();

	system("PAUSE");
	return 0;
}