#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "Figura.h"
#include <iostream>

class Kolo : public Figura
{
public:
	Kolo() : Kolo(1)
	{ }

	Kolo(double przekatna)
	{
		ustawPrzekatna(przekatna);
	}

	double obliczPole()
	{
		return M_PI * przekatna * przekatna;
	}

	void wypiszNazwe()
	{
		std::cout << "Kolo";
	}

	double pobierzPrzekatna()
	{
		return przekatna;
	}

	void ustawPrzekatna(double przekatna)
	{
		this->przekatna = przekatna > 0 ? przekatna : 1;
	}

private:
	double przekatna;
};