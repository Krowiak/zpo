--Zad.9
--Aby uruchomić w interpreterze potrzebne :set -XMultiParamTypeClasses i :set -XFlexibleInstances
import Kontener

data Zbior a = Z [a] deriving Show

instance (Ord a) => Kontener Zbior a where
    pusty = Z []
    dodaj x z
        | juzZawiera = z
        | otherwise  = Z (x:zs)
        where juzZawiera = x `elem` zs
              Z zs = z
    usun z = Z $ filter (/= (pierwszy z)) zs
        where Z zs = z
    pierwszy (Z xs) = minimum xs
    czyPusty (Z []) = True
    czyPusty (Z _ ) = False