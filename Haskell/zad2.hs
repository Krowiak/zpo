--Zad.2
r :: Int -> [Int]
r 0 = []
r x = (r $ x - 1) ++ (take x $ repeat x)