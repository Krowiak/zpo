--Zad.7
--Aby uruchomić w interpreterze potrzebne :set -XMultiParamTypeClasses i :set -XFlexibleInstances

--module Kontener ( Stos(),
--                  Kolejka(),
--                  pusty,
--                  dodaj,
--                  usun,
--                  pierwszy,
--                  czyPusty
--                ) where
--

class Kontener k a where
    pusty :: k a
    dodaj :: a -> k a -> k a
    usun :: k a -> k a
    pierwszy :: k a -> a
    czyPusty :: k a -> Bool

data Stos a = S [a] deriving Show

instance Kontener Stos a where
    pusty = S []
    dodaj x (S xs) = S (x:xs)
    usun (S (_:xs)) = S xs
    pierwszy (S (x:_)) = x
    czyPusty (S []) = True
    czyPusty (S _ ) = False

data Kolejka a = Q ([a],[a]) deriving Show

instance Kontener Kolejka a where
    pusty = Q ([],[])
    dodaj x (Q ([],[])) = Q ([x],[])
    dodaj y (Q (xs,ys)) = Q (xs,y:ys)
    usun (Q ([],ys)) = Q (tail (reverse ys), [])
    usun (Q (x:xs,ys)) = Q (xs,ys)
    pierwszy (Q ([],ys)) = last ys
    pierwszy (Q (x:xs,ys)) = x
    czyPusty (Q ([],[])) = True
    czyPusty _ = False

main = do
    putStrLn $ show $ dodaj 1 (dodaj 2 (dodaj 3 (pusty::(Stos Int))))
    putStrLn $ show $ dodaj "Ala" (dodaj "ma" (dodaj "kota" (pusty::(Kolejka String))))