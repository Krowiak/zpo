--Zad.4
zawiera :: Eq a => [a] -> [a] -> Bool
zawiera [] _ = True
zawiera (x:xs) ys
    | x `elem` ys = zawiera xs ys
    | otherwise   = False