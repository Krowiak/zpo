--Zad.5
data Drzewo = Nil | Wierzcholek Int Drzewo Drzewo deriving (Show)
wywazone :: [Int] -> Drzewo
wywazone [] = Nil
wywazone (x:xs) = Wierzcholek x (wywazone lewaPolowa) (wywazone prawaPolowa)
    where (lewaPolowa, prawaPolowa) = splitAt (length xs `div` 2) xs