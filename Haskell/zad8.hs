--Zad.8
--Aby uruchomić w interpreterze potrzebne :set -XMultiParamTypeClasses i :set -XFlexibleInstances
import Kontener

doOstatniego k
    | pusty = k
    | nastPusty = k
    | otherwise = doOstatniego k'
    where pusty = czyPusty k  --pierwsze wywołanie z pustym kontenerem
          k' = usun k
          nastPusty = czyPusty k'