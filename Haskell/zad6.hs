--Zad.6
f :: Int -> Int -> [[Int]]
f n k 
    | k > n = []
    | k == 1 = [[n]]
    | otherwise = [i:xs | i <- [1..n-k+1], xs <- f (n-i) (k-1)]

main = do putStrLn "Rozkład liczby na n składników"