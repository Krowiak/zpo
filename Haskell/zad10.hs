--Zad. 10
st xs = stopien xs []
    where 
    stopien lista@(x:xs) acc
        | rowne lista = length acc
        | otherwise   = stopien (roznice xs) (lista:acc)
        where
        rowne (x:xs) = all (==x) $ take 10 xs
        roznice [] = []
        roznice [x] = []
        roznice (x:xs@(y:zs)) = (x - y) : (roznice xs)
