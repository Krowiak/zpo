package zpo;

public class Ciężarówka extends Samochód {
    private int nośność;

    public Ciężarówka(String marka, int vMax, int nośność) {
        super(marka, vMax);
        this.nośność = nośność;
    }

    public Ciężarówka(Ciężarówka oryginal) {
        super(oryginal);
        this.nośność = oryginal.nośność;
    }

    @Override
    public void wyswietlDane() {
        super.wyswietlDane();
        System.out.println("Nośność: " + nośność);
    }
}
