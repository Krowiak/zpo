package zpo;

public class Main {

    public static void main(String[] args) {
        Samochód s = new Samochód("Samochód", 5);
        s.wyswietlDane();
        new Samochód(s).wyswietlDane();
        new Ciężarówka("Ciężarówka", 50, 1).wyswietlDane();
        Autobus a = new Autobus("Autobus", 11, 999);
        a.wyswietlDane();
        new Samochód(a).wyswietlDane();
    }
}
