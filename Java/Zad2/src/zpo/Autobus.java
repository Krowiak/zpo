package zpo;

public class Autobus extends Samochód {
    private int lMiejsc;

    public Autobus(String marka, int vMax, int lMiejsc) {
        super(marka, vMax);
        this.lMiejsc = lMiejsc;
    }

    public Autobus(Autobus oryginal) {
        super(oryginal);
        this.lMiejsc = oryginal.lMiejsc;
    }

    @Override
    public void wyswietlDane() {
        super.wyswietlDane();
        System.out.println("lMiejsc: " + lMiejsc);
    }
}
