package zpo;

public class Samochód {
    /*
    Napisać klasę Samochód zawierającą pola marka (łańcuch
znaków) i vMax (liczba całkowita) z konstruktorami: inicjującym
dane na wartości z parametrów i kopiującym dane oraz metodą
wypisującą dane na ekran. Zdefiniować klasy pochodne: Autobus,
dodającą pole lMiejsc (liczba całkowita) oraz Ci˛e˙ zarówka, dodającą
pole no´ sno´ s´ c (liczba całkowita). Klasy pochodne powinny zawierać
odpowiednie konstruktory oraz metody wypisujące dane na ekran.
     */

    private String marka;
    private int vMax;

    public Samochód(String marka, int vMax) {
        this.marka = marka;
        this.vMax = vMax;
    }

    public Samochód(Samochód oryginal) {
        this.marka = oryginal.marka;
        this.vMax = oryginal.vMax;
    }

    public void wyswietlDane() {
        System.out.println("Marka: " + marka);
        System.out.println("vMax: " + vMax);
    }
}
