package zpo;

public class TDate {
    private static int minRok = 0;

    private int dzien;
    private int miesiac;
    private int rok;

    public TDate()
    {
        this(0, 0, 0);
    }

    public TDate(int dzien, int miesiac, int rok)
    {
        if(Wpisz(dzien, miesiac, rok) != 0)
        {
            throw new IllegalArgumentException("Nie można stworzyć takiej daty.");
        }
    }

    public int Wpisz(int dd, int mm, int rr)
    {
        if(rr < minRok ||mm < 1 || mm > 12 || dd < 1 || dd > OstatniDzienMiesiaca(mm, rr))
        {
            return 1;
        }

        dzien = dd;
        miesiac = mm;
        rok = rr;
        return 0;
    }

    public void Wyswietl()
    {
        System.out.println(String.format("%1$02d-%2$02d-%3$04d", dzien, miesiac, rok));
    }

    private int OstatniDzienMiesiaca(int miesiac, int rok)
    {
        switch (miesiac)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                return JestPrzestepny(rok) ? 29 : 28;
            default:
                return 30;
        }
    }

    private boolean JestPrzestepny(int rok)
    {
        return rok % 400 == 0 || ((rok % 4 == 0) && rok % 100 != 0);
    }



}
