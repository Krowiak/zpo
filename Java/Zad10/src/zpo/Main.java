package zpo;

public class Main {

    public static void main(String[] args) {
        KlG p1 = new KlG();// KlG p1 = new KlC();  // Niekompatybilne klasy
        KlG p2 = new KlG();// KlG p2 = new IntU(); // Nie można tworzyć obiektu interfejsu, musi być zaimplementowany.
        IntU p3; // IntU p3 = new IntU(); // Nie można tworzyć obiektu interfejsu, musi być zaimplementowany.
        IntU p4 = new KlC();
        KlG p5 = new KlG(); // KlG p5 = new IntU() // Brak średnika; Nie można tworzyć obiektu interfejsu, musi być zaimplementowany.
        IntU p6 = p5; //IntU p6 = new p5; // new tworzy nowy obiekt klasy, nie przypisuje obiektu.
        IntU p7;
        p7 = new KlC();
    }
}
