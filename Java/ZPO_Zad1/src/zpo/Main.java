package zpo;

public class Main {

    public static void main(String[] args) {
        Rownanie r = new Rownanie(4, 2, 1);
        System.out.println("Wartość 4x^2 + 2x + 1 dla x = 5 wynosi: " + r.obliczDla(5));
        System.out.println("Liczba miejsc zerowych tego równiania wynosi: " + r.obliczLiczbeMiejscZerowych());
    }
}
