package zpo;

public class Rownanie {
    private double a;
    private double b;
    private double c;

    public Rownanie(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double obliczDla(double x) {
        return  a * x * x + b * x + c;
    }

    public int obliczLiczbeMiejscZerowych() {
        double delta = obliczDelte();
        return delta > 0 ? 2 : (delta == 0 ? 1 : 0);
    }

    private double obliczDelte() {
        return b * b - 4 * a * c;
    }
}
