package zpo;

public class Pojazd {
    private String nrRejestracyjny;

    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
        if(nrRejestracyjny.length() != 7) {
            throw new java.lang.IllegalArgumentException("Nr rejestracyjny musi mieć dokładnie 7 znaków.");
        }

        this.nrRejestracyjny = nrRejestracyjny;
    }
}
