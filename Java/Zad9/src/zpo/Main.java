package zpo;

public class Main {

    public static void main(String[] args) {
        Pojazd p = new Pojazd();
        try {
            p.setNrRejestracyjny("123");
        } catch (java.lang.IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
        p.setNrRejestracyjny("1234567");
        System.out.println(p.getNrRejestracyjny());

        Samochód s = new Samochód();
        try {
            s.setMarka("");
        } catch (java.lang.IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
        s.setMarka("12345");
        System.out.println(s.getMarka());
    }
}
