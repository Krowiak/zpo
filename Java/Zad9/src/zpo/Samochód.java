package zpo;

public class Samochód extends Pojazd {
    private String marka;

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        if(marka.length() < 1 || marka.length() > 20) {
            throw new java.lang.IllegalArgumentException("Marka musi mieć co najmniej 1 i nie więcej niż 20 znaków.");
        }
        this.marka = marka;
    }
}
