package zpo;


import java.util.ArrayList;

public class PosortowanaKolejka {
    private int rozmiar;
    private ArrayList<Integer> elementy;

    public PosortowanaKolejka(int rozmiar) {
        this.rozmiar = rozmiar;
        elementy = new ArrayList<Integer>(rozmiar);
    }

    public PosortowanaKolejka(PosortowanaKolejka oryginal) {
        elementy = (ArrayList<Integer>)oryginal.elementy.clone();
        rozmiar = oryginal.rozmiar;
    }

    public void Wstaw(int element) {
        if(elementy.size() >= rozmiar) {
            throw new RuntimeException("Przekroczono rozmiar kolejki.");
        }

        boolean dodano = false;
        for(int i = 0; i < elementy.size() && !dodano; i++) {
            if(elementy.get(i) >= element) {
                elementy.add(i, element);
                dodano = true;
            }
        }

        if(!dodano) {
            elementy.add(element);
        }
    }

    public void Usuń(int element) {
        for(int i = 0; i < elementy.size(); i++) {
            if(elementy.get(i) == element){
                elementy.remove(i);
                break;
            }
        }
    }

    public int Pobierz() {
        if(elementy.size() < 1) {
            throw new RuntimeException("Brak elementów w kolejce.");
        }

        int element = elementy.get(0);
        elementy.remove(0);
        return element;
    }

    public int Pobierz(int element) {
        for(int i = 0; i < elementy.size(); i++) {
            if(elementy.get(i) == element) {
                elementy.remove(i);
                return element;
            }
        }

        throw new RuntimeException("Brak elementu w kolejce");
    }
}
