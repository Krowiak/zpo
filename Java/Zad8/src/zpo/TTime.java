package zpo;

public class TTime {
    private int godzina;
    private int minuta;
    private int sekunda;

    public TTime() {
        this(12, 0, 0);
    }

    public TTime(int godzina, int minuta, int sekunda) {
        this.godzina = godzina;
        this.minuta = minuta;
        this.sekunda = sekunda;
    }

    public void wyswietlCzas() {
        System.out.println(String.format("%02d:%02d:%02d", godzina, minuta, sekunda));
    }

    public int getGodzina() {
        return godzina;
    }

    public void setGodzina(int godzina) {
        if(godzina < 0 || godzina > 23) {
            throw new java.lang.IllegalArgumentException("Godzina musi mieścić sie w zakresie 0-23");
        }
        this.godzina = godzina;
    }

    public int getMinuta() {
        return minuta;
    }

    public void setMinuta(int minuta) {
        if(minuta < 0 || minuta > 59) {
            throw new java.lang.IllegalArgumentException("Minuta musi mieścić sie w zakresie 0-59");
        }
        this.minuta = minuta;
    }

    public int getSekunda() {
        return sekunda;
    }

    public void setSekunda(int sekunda) {
        if(sekunda < 0 || sekunda > 59) {
            throw new java.lang.IllegalArgumentException("Sekunda musi mieścić sie w zakresie 0-59");
        }
        this.sekunda = sekunda;
    }
}
