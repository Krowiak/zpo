package zpo;

public class Stos {
    private double[] dane;
    private int pozycja;

    public Stos(int rozmiar) {
        dane = new double[rozmiar];
        pozycja = -1;
    }

    public Stos(Stos oryginal) {
        dane = oryginal.dane.clone();
        pozycja = oryginal.pozycja;
    }

    public void Push(double element) {
        pozycja++;
        dane[pozycja] = element;
    }

    public double Pop() {
        double element = dane[pozycja];
        pozycja--;
        return element;
    }
}
