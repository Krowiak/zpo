package zpo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {

    public static void Metoda(File file)
    {
        RandomAccessFile input = null;
        String wiersz = null;
        try {
            input = new RandomAccessFile( file, "r");
            while (( wiersz = input.readLine()) != null )
                System.out.println( wiersz );
            return;
        }
        catch(FileNotFoundException fnfe) {
            System.out.println("Plik nie istnieje.");
        }
        catch(IOException ioe) {
            System.out.println("Nie udało się odczytać pliku.");
        }
        finally
        {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ioe) {
                    System.out.println("Nie udało się zamknąć pliku.");
                }
            }
        }
    }

    public static void main(String[] args) {
        File f = new File("K:\\tórynie\\Istnieje");
        Metoda(f);
    }
}
