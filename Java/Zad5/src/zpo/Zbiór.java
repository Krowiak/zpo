package zpo;

import java.util.ArrayList;

public class Zbiór {
    private ArrayList<Byte> dane;

    public Zbiór() {
        dane = new ArrayList<Byte>();
    }

    public Zbiór(Zbiór oryginal) {
        dane = (ArrayList<Byte>)oryginal.dane.clone();
    }

    public void Suma(Zbiór dodawany) {
        for(byte b : dodawany.dane) {
            DodajElement(b);
        }
    }

    public void DodajElement(byte element) {
        if(!dane.contains(element)) {
            dane.add(element);
        }
    }

    public void UsunElement(byte element) {
        if(dane.contains(element)) {
            dane.remove(dane.indexOf(element));
        }
    }

    public void Iloczyn(Zbiór mnozony) {
        for(int i = 0; i < dane.size(); i++) {
            if(!mnozony.dane.contains(dane.get(i))) {
                UsunElement(dane.get(i));
            }
        }
    }

    public void Różnica(Zbiór odejmowany) {
        for(byte doOdjecia : odejmowany.dane) {
            UsunElement(doOdjecia);
        }
    }

    public void CzyscZbior() {
        dane.clear();
    }

    public boolean CzyWZbiorze(byte element) {
        return dane.contains(element);
    }
}
