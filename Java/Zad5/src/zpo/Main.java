package zpo;

public class Main {

    public static void main(String[] args) {
        Zbiór a = new Zbiór();
        a.DodajElement((byte)1);
        a.DodajElement((byte)2);
        a.DodajElement((byte)4);
        a.DodajElement((byte)6);
        a.DodajElement((byte)8);
        a.DodajElement((byte)11);

        Zbiór b = new Zbiór(a);
        System.out.println(b.CzyWZbiorze((byte)2));
        System.out.println(b.CzyWZbiorze((byte)3));
        b.DodajElement((byte)3);
        b.Iloczyn(a);
        System.out.println(b.CzyWZbiorze((byte)2));
        System.out.println(b.CzyWZbiorze((byte)3));
        b.DodajElement((byte)3);
        a.Suma(b);
        System.out.println(a.CzyWZbiorze((byte)3));
    }
}
